document.querySelector('.embedded-content').addEventListener("load", ev => {
    const new_style_element = document.createElement("style");
    new_style_element.textContent = ".wy-nav-top { display: none; } footer { display: none; } ul.wy-breadcrumbs {display: none;}"
    ev.target.contentDocument.head.appendChild(new_style_element);
});


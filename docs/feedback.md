<img src="https://gitlab.com/regenscore/template/-/raw/783f794435bf71e41f381b80b422c30f904fc6c2/docs/img/RegenScore-Logo.png" style="width:500px;height:80" alt="centered image"> 

## **Verification Standard - Community Review**
Through a scientifically rigorous and community-supported process, the RegenScore™ Alliance has developed a scoring framework and written verification standard for annual cropping systems in California. This framework and standard provide the foundation on which regionally specific adaptations will be made for additional cropping and other production systems. 

With support from OpenTeam, RegenScore™ Alliance is conducting a comprehensive, community review of its v1.0 verification standard (for Cropland Agriculture). We openly invite and would greatly appreciate your participation in the below survey. Alternatively or in addition, we invite you to provide line item feedback on the document at the following link. 

Your feedback will contribute directly to the improvement and evolution of the RegenScore framework. This initial round of community review (open to friends and family!) will extend from now until July 14, 2024.  We will follow-up shortly after this for additional feedback on the review process itself. Over the subsequent 4-6 weeks, your feedback (on both the standard and the process) will be synthesized and integrated into updated versions before releasing the standard and survey for public comment on or before September 1st, 2024.  

Thank you for your generous support of this work! 

### [Click here to view the Verification Standard.](https://nam11.safelinks.protection.outlook.com/?url=https%3A%2F%2Fdocs.google.com%2Fdocument%2Fd%2F1b2lSGxYz4nTFWjZCpsyhjuaAQod_ef3z%2Fedit&data=05%7C02%7Cgrichardson%40pointblue.org%7Cc7d537a5db7b4c4989bc08dc8b295195%7C0131db1e582f4fe9969fd1fc43e48f5f%7C0%7C0%7C638538256808646443%7CUnknown%7CTWFpbGZsb3d8eyJWIjoiMC4wLjAwMDAiLCJQIjoiV2luMzIiLCJBTiI6Ik1haWwiLCJXVCI6Mn0%3D%7C0%7C%7C%7C&sdata=KTHRT2AN3AFwRviz1q85hhKLZo20aCUaEoNeEAs2Kro%3D&reserved=0)


### Feedback Form 
[(link to full-page form)](https://app.surveystack.io/groups/600f3077e51f5d000136b258/surveys/6669f97a5680b30d505f8b1a/submissions/new)



<iframe width="1460" height="1000" src="https://app.surveystack.io/groups/600f3077e51f5d000136b258/surveys/6669f97a5680b30d505f8b1a/submissions/new"  title="RegenScore Cropland Verification Standard v0.1 Feedback Form" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>